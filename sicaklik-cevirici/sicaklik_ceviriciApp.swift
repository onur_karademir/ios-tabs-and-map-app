//
//  sicaklik_ceviriciApp.swift
//  sicaklik-cevirici
//
//  Created by Onur on 21.05.2022.
//

import SwiftUI

@main
struct sicaklik_ceviriciApp: App {
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem{
                        HStack{
                            Image(systemName: "thermometer")
                            Text("Convrsion")
                        }
                    }
                MapView()
                    .tabItem{
                        HStack{
                            Image(systemName: "map")
                            Text("Map")
                        }
                    }
            }
            .accentColor(.purple)
        }
    }
}
