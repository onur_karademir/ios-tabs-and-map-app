//
//  MapView.swift
//  sicaklik-cevirici
//
//  Created by Onur on 21.05.2022.
//

import SwiftUI
import MapKit

struct MapView: View {
    
    static var regions: [MKCoordinateRegion] = [
        MKCoordinateRegion(
            
            center: CLLocationCoordinate2D(
                latitude: 41.0,
                longitude: 29.0),
            
            span: MKCoordinateSpan(
                latitudeDelta: 1.0,
                longitudeDelta: 1.0)
        
        ),
        
        MKCoordinateRegion(
            
            center: CLLocationCoordinate2D(
                latitude: 51.509865,
                longitude: -0.118092),
            
            span: MKCoordinateSpan(
                latitudeDelta: 1.0,
                longitudeDelta: 1.0)
        
        )
    
    ]
    
    @State var region : MKCoordinateRegion = regions[0]
    
    @State var selectionIndex = 0
    
    var body: some View {
        
        Map(coordinateRegion: $region)
            .edgesIgnoringSafeArea(.top)
            .overlay(
                VStack{
                    Picker("picker", selection: $selectionIndex, content: {
                        Text("istanbul").tag(0)
                        Text("london").tag(1)
                    })
                    .pickerStyle(SegmentedPickerStyle())
                    Spacer()
                    .padding()
                    .onChange(of: selectionIndex, perform: {
                        value in self.region = MapView.regions[selectionIndex]
                    })
                }
              
            )
        
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
