//
//  ContentView.swift
//  sicaklik-cevirici
//
//  Created by Onur on 21.05.2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var fahrenheitValue: String = ""
    
    func convertToCelc () -> String {
        
        if let value = Double(fahrenheitValue) {
            
            let fahrenheit = Measurement<UnitTemperature>(value:value, unit: .fahrenheit)
            let celsValue = fahrenheit.converted(to: .celsius)
            return "\(celsValue.value)"
        }else{
            return"???"
        }
        
        
    }

    var body: some View {
        VStack {
            
            TextField("value", text: $fahrenheitValue)
                .keyboardType(.decimalPad)
                .font(Font.system(size: 64))
                .multilineTextAlignment(.center)
            
            Text("fahrenheit")
            Text("is actully")
                .foregroundColor(.gray)
            
            Text(convertToCelc())
                .font(Font.system(size: 64))
            Text("degres Celcius")
            Spacer()
            
        }.font(.title).foregroundColor(.orange)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
